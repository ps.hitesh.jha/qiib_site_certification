# Steps to Push Image to the OCP registry:

**1) Copy the image tar to one of the node**

`[root@bastion mpay]# scp ui.tar core@172.21.124.205:/var/home/core/`

**2) Login to the particular node**

`[root@bastion mpay]# ssh core@172.21.124.205`

**3) Load the image to podman**

`[root@bastion mpay]# podman load < ui.tar`

**4) Tag the image as below**

`[root@bastion mpay]# podman tag <image-loaded> <registryname>:<registryport>/<namespace>/<image_name>:<image_tag>`

**5) Login to the ocp**

`[root@bastion mpay]# oc login https://api.mpay.qiibonline.com:6443`

**6) Login to the podman registry**

`[root@bastion mpay]# podman login -u kubeadmin -p $(oc whoami -t) image-registry.openshift-image-registry.svc:5000`

**7) Push the image** 

`[root@bastion mpay]# podman push <image>`

# Steps to execute the docker image:
`helm upgrade --install --set global.keystore=$(cat /mpay/Application/qiibkeystore.jks| base64 -w 0) mpay /mpay/Application/'Helm Charts'/mpay_v4.0.0_ssl`
